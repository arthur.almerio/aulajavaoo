package main;

import java.util.Scanner;

public class Main {

	static Scanner key = new Scanner(System.in);

	public static void main(String[] args) {
		ex1();
		ex2();
		ex3();
		ex4();
		ex5();
		ex6();
		ex7();
		ex8();
		ex9();
		ex10();
		ex11();
		ex12();
		ex13();
		ex14();
		ex15();
	}

	public static void ex1() {
		System.out.println("Digite numero:");
		int n1 = key.nextInt();
		System.out.println("Digite numero:");
		int n2 = key.nextInt();
		System.out.println("A soma dos numeros:" + (n1 + n2));
	}

	public static void ex2() {
		System.out.println("Digite numero:");
		double n1 = key.nextDouble();
		System.out.println("Digite numero:");
		double n2 = key.nextDouble();
		System.out.println("A soma dos numeros:" + (n1 + n2));
		System.out.println("A subtracao dos numeros:" + (n1 - n2));
		System.out.println("A multiplicacao dos numeros:" + (n1 * n2));
		System.out.println("A divisao dos numeros:" + (n1 / n2));
	}

	public static void ex3() {
		System.out.println("Digite km rodado:");
		double n1 = key.nextDouble();
		System.out.println("Digite litros abastecido:");
		double n2 = key.nextDouble();
		System.out.println("Carro fez:" + (n1 / n2) + "km/l");
	}

	public static void ex4() {
		System.out.println("Digite nome vendedor:");
		String nome = key.nextLine();
		System.out.println("Digite salario:");
		double n1 = key.nextDouble();
		System.out.println("Digite vendas:");
		double n2 = key.nextDouble() * 0.15;
		System.out.println("Nome: " + nome);
		System.out.println("Salario: " + n1);
		System.out.println("Salario final: " + (n1 + n2));
	}

	public static void ex5() {
		System.out.println("Nome aluno: ");
		System.out.println("Digite 1 nota: ");
		double n1 = key.nextDouble();
		System.out.println("Digite 2 nota: ");
		double n2 = key.nextDouble();
		System.out.println("Digite 3 nota: ");
		double n3 = key.nextDouble();
		System.out.println("Media aluno: " + ((n1 + n2 + n3) / 3));
	}

	public static void ex6() {
		System.out.println("Digite 1 valor:");
		String a = key.nextLine();
		System.out.println("Digite 2 valor: ");
		String b = key.nextLine();
		String c = a;
		a = b;
		b = c;
		System.out.println("A: " + a);
		System.out.println("B: " + b);
	}

	public static void ex7() {
		System.out.println("Temperatura em C�: ");
		double c = key.nextDouble();
		System.out.println("Temperatura Fahrenheit: " + ((9 * c + 160) / 5));
	}

	public static void ex8() {
		System.out.println("Valor em R$: ");
		double reais = key.nextDouble();
		System.out.println("Cota��o dolar: ");
		double cota = key.nextDouble();
		System.out.println("Total em US$: " + (reais / cota));
	}

	public static void ex9() {
		System.out.println("Valor a ser depositado: ");
		double valor = key.nextDouble();
		System.out.println("Valor rendido pos 1 mes: " + (valor * 1.007));
	}

	public static void ex10() {
		System.out.println("Valor da compora: ");
		double valor = key.nextDouble();
		System.out.println("5 parcelas de R$: " + (valor / 5));
	}

	public static void ex11() {
		System.out.println("Pre�o de custo R$: ");
		double custo = key.nextDouble();
		System.out.println("Percentual: ");
		double por = key.nextDouble();
		System.out.println("Preco de venda R$: " + (custo * por / 100 + custo));
	}

	public static void ex12() {
		System.out.println("Raio circulo: ");
		double raio = key.nextDouble();
		System.out.println("Area circulo: " + (3.14 * (raio * raio)));
	}

	public static void ex13() {
		System.out.println("Digite numero");
		int num = key.nextInt();
		if (num > 20) {
			System.out.println("Numero maior que 20!");
		}
	}

	public static void ex14() {
		System.out.println("Digite 1 numero: ");
		int n1 = key.nextInt();
		System.out.println("Digite 2 numero: ");
		int n2 = key.nextInt();
		if (n1 > n2) {
			System.out.println("Numero 2: " + n2 + ", menor que numero 1: " + n1);
		} else {
			System.out.println("Numero 1: " + n1 + ", menor que numero 2: " + n2);
		}
	}

	public static void ex15() {
		System.out.println("Digite 1 numero: ");
		int n1 = key.nextInt();
		System.out.println("Digite 2 numero: ");
		int n2 = key.nextInt();
		if (n1 > n2) {
			System.out.println(n1 + "," + n2);
		} else {
			System.out.println(n2 + "," + n1);
		}
	}
}
